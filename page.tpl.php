<?php
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
<meta charset="utf-8" />

 <title><?php print $head_title; ?></title>
 <?php print $head; ?>
 <?php print $styles; ?>
 <?php print $scripts; ?>

</head>

<body id="index" class="home">

 <div id="container">

   <div id="header">

     <?php if ($logo): ?>
     <a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a>
     <?php endif; ?>

   </div>

    <div id="navigation">
   
     <div id="primary">
       <?php print theme('links', $primary_links); ?>
     </div> <!-- /#primary -->

    </div>

    <div class="clear">
    </div>

    <div id="contentleft">
      <!-- <?php print $breadcrumb; ?> -->
      <?php if ($mission): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>

      <div id="content">
        <?php if ($title): ?><h1 class="title"><?php print $title; ?></h1><?php endif; ?>
        <?php if ($tabs): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
        <?php if ($show_messages): print $messages; endif; ?>
        <?php print $help; ?>
        <?php print $content; ?>

      </div>

    </div>

    <div id="contentright">

      <div id="mediabox">
        <?php print $mediabox; ?>
      </div>

      <div class="sidebaritem">
        <?php print $sidebaritem; ?>
      </div>

    </div>

    <div class="clear";>
    </div>

    <div id="footer">
      <?php if ($footer_message || $footer) : ?>

        <div id="footer-message">
          <?php print $footer_message . $footer;?>
        </div>
      <?php endif; ?>

    </div>

  </div>

  <?php print $closure; ?>
</body>
</html>